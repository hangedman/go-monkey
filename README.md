# go-monkey

Golang lexer/parser learning scripts

## develop

```
$ mkdir -p ~/.go/src/bitbucket.org/hangedman/
$ cd ~/.go/src/bitbucket.org/hangedman/
$ git clone git@bitbucket.org:hangedman/go-monkey.git
```

## test

```
$ go test ./lexer
```
